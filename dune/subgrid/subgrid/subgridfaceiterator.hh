// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_SUBGRID_SUBGRID_FACE_ITERATOR_HH
#define DUNE_SUBGRID_SUBGRID_FACE_ITERATOR_HH

#include <dune/geometry/referenceelements.hh>

#include <dune/subgrid/subgrid/subgridentity.hh>

namespace Dune {

// forward declaration
template <class GridImp> class SubGridLeafIntersection;

/** \brief A base class for face iterators on a subgrid. */
template <class GridImp, class IteratorType, class IntersectionType>
class SubGridFaceIterator :
    public Dune::ForwardIteratorFacade<IteratorType, const IntersectionType>
{

    enum {dim=GridImp::dimension};
    typedef typename GridImp::ctype ctype;

public:
    typedef typename GridImp::HostGridType::template Codim<0>::Entity HostElement;
    typedef typename GridImp::HostGridType::LevelGridView::IntersectionIterator HostLevelIntersectionIterator;

    //! Default Constructor.
    SubGridFaceIterator()
    {}

    SubGridFaceIterator(const GridImp* g) :
        subGrid_(g)
    {}

protected:

    /** \brief Return true if father is a father face of son.
    */
    static bool isSubFace(const HostElement& father, const int fatherFace, const HostElement& son, const int sonFace)
    {
        const auto& fatherRefElement = Dune::ReferenceElements<ctype, dim>::general(father.type());
        const auto& sonRefElement = Dune::ReferenceElements<ctype, dim>::general(son.type());

        auto&& centerInFather = son.geometryInFather().global(sonRefElement.position(sonFace, 1));

        const auto& geometry = fatherRefElement.template geometry<1>(fatherFace);

        centerInFather -= geometry.global(geometry.local(centerInFather));

        return centerInFather.two_norm2() < 1e-4;
    }

    const GridImp* subGrid_;
};


/** \brief A hierarchical iterator for faces
 *
 * Starting from a start face the iterator performes a depth-first
 * search for elements e containing a subface in the hierarchic
 * element tree.
 *
 * Subfaces are computed using the (semi-)topological information
 * provided by geometryInFather() and the subentity embedding of
 * the reference elements. Thus you can also use the iterator
 * for grids with parametrized boundaries.
 *
 * If an element contains no subface the subtree of its descendents
 * is skipped.
 * Hence the overall complexity for iterating over all subfaces
 * is O(r n ) where 'r' is the maximal number of children
 * obtained by refining an element and 'n' is the number
 * of subfaces.
 *
 * The memory requirement is O(maxLevel-level(start face)).
 *
 * There are some case where the hierarchic search skips the children of a subface:
 *
 * 1. If we found an intersection with a subgrid leaf element we don't have to look in the children tree
 * 2. If we found an intersection with an element that is not contained in the subgrid we also may stop
 *
 * Furthermore the possiblity is added to create "singleton" iterators, that is an intersection wrapped to a face iterator
 * In this case we simulate that the incremented iterator is the end iterator, so the hierarchy only consists of the single face
 *
 */
template<class GridImp>
class SubGridHierarchicLeafFaceIterator :
    public SubGridFaceIterator<GridImp, SubGridHierarchicLeafFaceIterator<GridImp>, typename GridImp::LeafIntersection>
{
    enum {dim=GridImp::HostGridType::dimension};

    typedef SubGridHierarchicLeafFaceIterator<GridImp> This;
    typedef SubGridFaceIterator<GridImp,This,typename GridImp::LeafIntersection> Base;
    typedef typename GridImp::HostGridType HostGridType;
    typedef typename HostGridType::HierarchicIterator HierarchicElementIterator;
    typedef typename HostGridType::template Codim<0>::Entity HostElement;
    typedef typename HostGridType::LevelIntersectionIterator LevelIntersectionIterator;
    typedef typename HostGridType::LevelIntersection HostLevelIntersection;
    typedef SubGridEntity<0,dim,GridImp> SubEntity;
    typedef SubGridLeafIntersection<GridImp> LeafIntersectionImpl;
    typedef typename GridImp::LeafIntersection Intersection;

    using Base::subGrid_;

public:

    enum PositionFlag {begin, end};

    SubGridHierarchicLeafFaceIterator()
    {}

    /** \brief An intersection with a coarser element wrapped as a leaf face iterator */
    SubGridHierarchicLeafFaceIterator(const GridImp* subGrid, HostLevelIntersection insideIntersection,
                           HostElement inside, HostElement outside,
                            HostLevelIntersection outsideIntersection) :
        Base(subGrid),
        intersect_(LeafIntersectionImpl(subGrid_,std::move(insideIntersection),
                    std::move(inside),std::move(outside),std::move(outsideIntersection))),
        flag_(begin)
    {}

    /** \brief An regular intersection of two leaf elements wrapped as a leaf face iterator. */
    SubGridHierarchicLeafFaceIterator(const GridImp* subGrid, HostLevelIntersection intersection,
                           HostElement inside, HostElement outside) :
        Base(subGrid),
        intersect_(LeafIntersectionImpl(subGrid_,intersection,std::move(inside),
                                        std::move(outside), std::move(intersection))),
        flag_(begin)
    {}

    /** \brief A boundary intersection wrapped as a leaf face iterator. */
    SubGridHierarchicLeafFaceIterator(const GridImp* subGrid, HostLevelIntersection intersection,
                           HostElement inside) :
        Base(subGrid),
        intersect_(LeafIntersectionImpl(subGrid_,intersection,std::move(inside),
                                                std::move(intersection))),
        flag_(begin)
    {}

    /** \brief Begin constructor */
    SubGridHierarchicLeafFaceIterator(const GridImp* subGrid, HostLevelIntersection fatherFace) :
        Base(subGrid),
        fatherFace_(std::move(fatherFace)),
        flag_(begin)
    {
        stack_.reserve(subGrid_->maxLevel()-fatherFace_.inside().level());
        // start iterating from the flipped intersection
        const auto& outside = fatherFace_.outside();
        int level = outside.level();
        stack_.push_back(ChildFaceIterator(subGrid_, outside.hbegin(level+1), outside.hend(level+1)));
        incrementFromStack();
    }

    /** \brief End constructor */
    SubGridHierarchicLeafFaceIterator(const GridImp* subGrid) :
        Base(subGrid),
        flag_(end)
    {}

    /** \brief Copy constructor */
    SubGridHierarchicLeafFaceIterator(const SubGridHierarchicLeafFaceIterator& other) :
        Base(other.subGrid_),
        fatherFace_(other.fatherFace_),
        intersect_(other.intersect_),
        stack_(other.stack_),
        flag_(other.flag_)
    {}

    /** \brief Move constructor */
    SubGridHierarchicLeafFaceIterator(SubGridHierarchicLeafFaceIterator&& other) :
        Base(other.subGrid_),
        fatherFace_(std::move(other.fatherFace_)),
        intersect_(std::move(other.intersect_)),
        stack_(std::move(other.stack_)),
        flag_(other.flag_)
    {}


    /** \brief Assignement operator */
    SubGridHierarchicLeafFaceIterator& operator=(const SubGridHierarchicLeafFaceIterator& other)
    {
        fatherFace_ = other.fatherFace_;
        intersect_ = other.intersect_;
        stack_ = other.stack_;
        flag_ = other.flag_;

        return *this;
    }

    /** \brief Assignement operator */
    SubGridHierarchicLeafFaceIterator& operator=(SubGridHierarchicLeafFaceIterator&& other)
    {
        fatherFace_ = std::move(other.fatherFace_);
        intersect_ = std::move(other.intersect_);
        stack_ = std::move(other.stack_);
        flag_ = other.flag_;

        return *this;
    }


    //! Destructor
    ~SubGridHierarchicLeafFaceIterator() {}

    /** \brief Increment the iterator */
    void increment()
    {
        if (stack_.size()>0) {

            // increment hierarchic iterator
            stack_.back().it_++;
            incrementFromStack();
        }
        else // if we are incrementing from an empty stack then we reached the enditerator
            flag_ = end;
    }

    /** \brief Compare to other iterator */
    bool equals(const SubGridHierarchicLeafFaceIterator& other) const
    {
        if ((flag_==end) && other.flag_ == end)
            return true;
        else
            return intersect_ == other.intersect_;
    }

    /** \brief Dereference the iterator */
    const Intersection& dereference() const
    {
        return intersect_;
    }

protected:

    /** \brief The following methods implement a depth first of
    // sub faces in all decendent elements as depicted
    // below:
    //
    // *-3-*-4-*-6-*-7-*
    // *---2---*---5---*
    // *-------1-------*
    //
    // All ancestors are stored in the stack in order
    // to visit their siblings later on.
    //
    // To this end we need to store the triple ChildFaceIterator
    // containing the element iterator, the face index, and the
    // end element iterator.
    */
    void incrementFromStack()
    {

        // Iterate on current level and go down in the tree
        // until we find a child face or reach the end (=stack is empty)
        do {
            // if hierarchical tree has been searched go up one level and increment
            while (stack_.back().it_ == stack_.back().end_)
            {
                stack_.pop_back();
                if (stack_.size() == 0) {
                    flag_=end;
                    return;
                }
                ++(stack_.back().it_);
            }
        } while (!findChildInBack());
    }

    /** \brief Search for a child face in back of stack
     * and set the intersection if a valid one is found.
     * Returns whether a child face was found, if none was found the
     * hierarchic elementer is incremented.
     */
    bool findChildInBack()
    {
        bool found = false;
        if (stack_.size() == 1)
            found = stack_.back().selectChildFaceOf(fatherFace_.outside(), fatherFace_.indexInOutside());
        else if (stack_.size() > 1) // check for other children in the previous element on the stack
            found = stack_.back().selectChildFaceOf(*stack_[stack_.size()-2].it_, stack_[stack_.size()-2].index_);
        if (found) {

            const HostElement& stackElement = *stack_.back().it_;

            // continue if inside is contained but not leaf in subgrid
            if (this->subGrid_->template isLeaf<0>(stackElement))
            {
                intersect_ = LeafIntersectionImpl(subGrid_,fatherFace_,
                                fatherFace_.inside(), stackElement, *stack_.back().toIntersect());
            }
            // currentFace is a boundary intersection if inside element is not subgrid entity
            else if (! this->subGrid_->template contains<0>(stackElement)) {
                intersect_ = LeafIntersectionImpl(subGrid_, fatherFace_,
                fatherFace_.inside(), *stack_.back().toIntersect());
            }
            else {
                int level = stackElement.level();
                // child of this face is a subface so go up the hierarchy for one level
                stack_.push_back(ChildFaceIterator(subGrid_, stackElement.hbegin(level+1),
                                                        stackElement.hend(level+1)));
                // to not skip any child elements call this function again here
                return findChildInBack();
            }
        } else // if now valid child was found then increment the hierarchic element iterator
           ++(stack_.back().it_);

        return found;
    }

    //! \brief encapsulate hierarchic iterator, end iterator and face index
    struct ChildFaceIterator
    {
        ChildFaceIterator(
                const GridImp* subGrid,
                const HierarchicElementIterator& it,
                const HierarchicElementIterator& end,
                int face = -1) :
            subGrid_(subGrid),
            it_(it),
            end_(end),
            index_(face)
        {}

        LevelIntersectionIterator toIntersect() const
        {
            LevelIntersectionIterator nIt = subGrid_->hostIIFactory().iLevelBegin(*it_);
            LevelIntersectionIterator nEnd = subGrid_->hostIIFactory().iLevelEnd(*it_);
            for(; nIt != nEnd; ++nIt)
            {
                if (nIt->indexInInside()==index_)
                    return nIt;
            }
            return nEnd;
        }

        bool selectChildFaceOf(const HostElement& father, int fatherFace)
        {
            index_ = -1;
            const HostElement& e = *it_;
            for(uint i=0; i < e.subEntities(1); ++i)
                if (Base::isSubFace(father, fatherFace, e, i)) {
                    index_ = i;
                    break;
                }

            return (index_ >= 0);
        }

        const GridImp* subGrid_;
        HierarchicElementIterator it_;
        HierarchicElementIterator end_;
        int index_;
    };

    //! the intersection we start from
    HostLevelIntersection fatherFace_;

    //! the intersection the iterator points to
    Intersection intersect_;

    //! a stack of hierarchic iterators with face indices
    std::vector<ChildFaceIterator> stack_;

    //! Flag to determine the end iterator
    PositionFlag flag_;
};


/** \brief A father iterator for SubGrid intersections. */
template <class GridImp>
class SubGridFatherFaceIterator :
    public SubGridFaceIterator<GridImp, SubGridFatherFaceIterator<GridImp>,
        typename GridImp::HostGridType::LevelIntersectionIterator::Intersection>
{
    typedef SubGridFatherFaceIterator<GridImp> This;
    typedef typename GridImp::HostGridType HostGridType;
    typedef typename HostGridType::template Codim<0>::Entity HostEntity;
    typedef typename HostGridType::LevelIntersectionIterator HostLevelIntersectionIterator;
    typedef typename HostLevelIntersectionIterator::Intersection HostLevelIntersection;
public:

    enum PositionFlag {begin, end};

    SubGridFatherFaceIterator(const GridImp* g, HostLevelIntersectionIterator startFace, PositionFlag flag) :
        SubGridFaceIterator<GridImp,This,HostLevelIntersection>(g),
        currentFace_(*startFace),
        flag_(flag)
    {}

    SubGridFatherFaceIterator(const GridImp* g, HostLevelIntersection startFace, PositionFlag flag) :
        SubGridFaceIterator<GridImp,This,HostLevelIntersection>(g),
        currentFace_(std::move(startFace)),
        flag_(flag)
    {}

        /** \brief Increment the iterator */
    void increment()
    {
        const HostEntity& currentInside = currentFace_.inside();
        // currentFace is the end if the inside element is already on level 0
        if (currentInside.level() == 0) {
            flag_=end;
            return;
        }

        // Get father intersection
        const HostEntity& fatherElement  = currentInside.father();

        // loop over all intersections of the father element and look for the father intersection
        HostLevelIntersectionIterator fatherFaceIt = this->subGrid_->hostIIFactory().iLevelBegin(fatherElement);
        HostLevelIntersectionIterator fatherFaceEnd = this->subGrid_->hostIIFactory().iLevelEnd(fatherElement);

        for (; fatherFaceIt != fatherFaceEnd; ++fatherFaceIt) {
            const HostLevelIntersection& fatherFace = *fatherFaceIt;
            if (this->isSubFace(fatherFace.inside(),fatherFace.indexInInside(),
                                currentFace_.inside(), currentFace_.indexInInside())) {
                currentFace_= fatherFace;
                return;
            }
        }

        // we didn't find a father face, so this is the end iterator now
        flag_ = end;
    }

    /** \brief Compare with other iterator */
    bool equals(const SubGridFatherFaceIterator& other) const
    {
        return (flag_==end && other.flag_==end) || (flag_ != end && other.flag_ != end &&  currentFace_ == other.currentFace_);
    }

    /** \brief Dereference the iterator */
    const HostLevelIntersection& dereference() const
    {
        return currentFace_;
    }

private:
    HostLevelIntersection currentFace_;
    PositionFlag flag_;
};

}  // namespace Dune

#endif // DUNE_SUBGRID_SUBGRID_FACE_ITERATOR_HH
