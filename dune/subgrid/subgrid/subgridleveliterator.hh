#ifndef DUNE_SUBGRIDLEVELITERATOR_HH
#define DUNE_SUBGRIDLEVELITERATOR_HH

#include <dune/grid/common/gridenums.hh>
#include <dune/subgrid/subgrid/subgridentityiterator.hh>

/** \file
* \brief The SubGridLevelIterator class and its specializations
*/

namespace Dune {




//**********************************************************************
//
// --SubGridLevelIterator
/**
 * \brief Iterator over all entities of a given codimension and level of a grid.
 * \ingroup SubGrid
 */
template<int codim, PartitionIteratorType pitype, class GridImp>
class SubGridLevelIterator :
    public Dune::SubGridEntityIterator<codim, GridImp, typename GridImp::HostGridType::Traits::template Codim<codim>::LevelIterator>
{
    private:

        typedef typename GridImp::HostGridType::Traits::template Codim<codim>::LevelIterator HostLevelIterator;
        typedef SubGridEntityIterator<codim, GridImp, HostLevelIterator> Base;

        using Base::subGrid_;
        using Base::hostIterator_;
        using Base::subGridContainsHostIterator;

    public:

        //! Default constructor
        SubGridLevelIterator()
        {}

        //! Constructor
        explicit SubGridLevelIterator(const GridImp* subGrid, int level) :
            Base(subGrid, subGrid->getHostGrid().levelGridView(level).template begin<codim>()),
            hostLevelEndIterator_(subGrid->getHostGrid().levelGridView(level).template end<codim>())
        {
            // Search for the first entity which is in the subgrid
            if (not subGridContainsHostIterator())
                increment();
        }


        /**
         * \brief Constructor which create the end iterator
         * 
         * \param endDummy Here only to distinguish it from the other constructor
         */
        explicit SubGridLevelIterator(const GridImp* subGrid, int level, bool endDummy DUNE_UNUSED) :
            Base(subGrid, subGrid->getHostGrid().levelGridView(level).template end<codim>()),
            hostLevelEndIterator_(hostIterator_)
        {}

        //! Copy Constructor
        SubGridLevelIterator(const SubGridLevelIterator& other) :
            Base(other.subGrid_, std::move(other.hostIterator_)),
            hostLevelEndIterator_(std::move(other.hostLevelEndIterator_))
        {}

        //! Move Constructor
        SubGridLevelIterator(SubGridLevelIterator&& other) :
            Base(other.subGrid_, std::move(other.hostIterator_)),
            hostLevelEndIterator_(std::move(other.hostLevelEndIterator_))
        {}


        //! Assignement operator
        SubGridLevelIterator& operator=(const SubGridLevelIterator& other)
        {
            subGrid_ = other.subGrid_;
            hostIterator_ = other.hostIterator_;
            hostLevelEndIterator_ = other.hostLevelEndIterator_;
            return *this;
        }


        //! Move assignement operator
        SubGridLevelIterator& operator=(SubGridLevelIterator&& other)
        {
            subGrid_ = other.subGrid_;
            hostIterator_ = std::move(other.hostIterator_);
            hostLevelEndIterator_ = std::move(other.hostLevelEndIterator_);
            return *this;
        }

        //! prefix increment
        void increment()
        {
            do
            {
                ++hostIterator_;
                if (hostIterator_ == hostLevelEndIterator_)
                    return;
            }
            while (not subGridContainsHostIterator());
        }


    private:

        /**
         * \brief Cached hostgrid end iterator
         *
         * We store a copy of the hostgrid end iterator since we need to access it several times
         */
        HostLevelIterator hostLevelEndIterator_;

};


}  // end namespace Dune

#endif
