#ifndef DUNE_SUBGRID_SUBGRID_LEAF_INTERSECTION_ITERATOR_HH
#define DUNE_SUBGRID_SUBGRID_LEAF_INTERSECTION_ITERATOR_HH

#include <list>
#include <tr1/memory>
#include <dune/common/shared_ptr.hh>

#include <dune/subgrid/subgrid/subgridfaceiterator.hh>

/** \file
 * \brief The SubGridLeafIntersectionIterator class
 */

namespace Dune {

/** \brief Iterator over all element neighbors
 * \ingroup SubGrid
 * Mesh entities of codimension 0 ("elements") allow to visit all neighbors, where
 * a neighbor is an entity of codimension 0 which has a common entity of codimension 1
 * These neighbors are accessed via an IntersectionIterator. This allows to implement
 * non-matching meshes. The number of neighbors may be different from the number
 * of an element!
 */
template<class GridImp>
class SubGridLeafIntersectionIterator
{

private:

    enum {dim=GridImp::dimension};

    enum {dimworld=GridImp::dimensionworld};

    // The type used to store coordinates
    typedef typename GridImp::ctype ctype;

    typedef typename GridImp::HostGridType::LevelIntersectionIterator HostLevelIntersectionIterator;
    typedef typename GridImp::HostGridType::LevelIntersection HostLevelIntersection;

    typedef typename GridImp::LeafIntersection Intersection;
    typedef SubGridHierarchicLeafFaceIterator<GridImp> HierarchicFaceIterator;
    typedef SubGridFatherFaceIterator<GridImp> FatherFaceIterator;
    typedef typename GridImp::HostGridType::template Codim<0>::Entity HostElement;

public:


    SubGridLeafIntersectionIterator()
    {}

    SubGridLeafIntersectionIterator(const GridImp* subGrid,
            HostLevelIntersectionIterator hostIterator,
            HostLevelIntersectionIterator endIterator)
        : subGrid_(subGrid),
        hostIterator_(std::move(hostIterator)), endIterator_(std::move(endIterator)),
        hierarchicFaceIterator_(subGrid_),
        endFaceIterator_(subGrid_)
    {
        // initialize the subintersection iterators
        initSubIntersections();
    }


    //! Copy constructor
    SubGridLeafIntersectionIterator(const SubGridLeafIntersectionIterator& other) :
        subGrid_(other.subGrid_),
        hostIterator_(other.hostIterator_),
        endIterator_(other.endIterator_),
        hierarchicFaceIterator_(other.hierarchicFaceIterator_),
        endFaceIterator_(other.endFaceIterator_)
    {}

    //! Move constructor
    SubGridLeafIntersectionIterator(SubGridLeafIntersectionIterator&& other) :
        subGrid_(other.subGrid_),
        hostIterator_(std::move(other.hostIterator_)),
        endIterator_(std::move(other.endIterator_)),
        hierarchicFaceIterator_(std::move(other.hierarchicFaceIterator_)),
        endFaceIterator_(std::move(other.endFaceIterator_))
    {}

    //! Assigment operator
    SubGridLeafIntersectionIterator& operator=(const SubGridLeafIntersectionIterator& other) {

        subGrid_ = other.subGrid_;
        hostIterator_ = other.hostIterator_;
        endIterator_ = other.endIterator_;
        hierarchicFaceIterator_ = other.hierarchicFaceIterator_;
        endFaceIterator_ = other.endFaceIterator_;
        return *this;
    }

    //! Move assigment operator
    SubGridLeafIntersectionIterator& operator=(SubGridLeafIntersectionIterator&& other) {

        subGrid_ = other.subGrid_;
        hostIterator_ = std::move(other.hostIterator_);
        endIterator_ = std::move(other.endIterator_);
        hierarchicFaceIterator_ = std::move(other.hierarchicFaceIterator_);
        endFaceIterator_ = std::move(other.endFaceIterator_);
        return *this;
    }


    ~SubGridLeafIntersectionIterator()
    {}

    /** \brief Create subgrid intersection that correspond to the current host grid intersection
     */
    void initSubIntersections()
    {
        if (hostIterator_ != endIterator_)
        {
            // make end iterator of the hierarchic face iterators
            endFaceIterator_ = HierarchicFaceIterator(subGrid_);

            // for some grids dereferencing returns an object, so call it once only
            HostLevelIntersection hostIntersection = *hostIterator_;
            // We need to distinguish several cases
            if (hostIntersection.neighbor())
            {
                HostElement outside = hostIntersection.outside();
                // Case A/B: There is a host neighbor that is contained in the subgrid
                if (subGrid_->template contains<0>(outside))
                {
                    // Case A: There is subgrid neighbor on the same level that is leaf
                    // We can use the intersection with this neighbor.
                    if (subGrid_->template isLeaf<0>(outside))
                        hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection), hostIntersection.inside(), std::move(outside));
                    // Case B: There is subgrid neighbor on the same level that is not leaf
                    // We have to find a leaf child containing the intersection
                    else
                        hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection));
                }
                // Case C: There is a host grid neighbor not contained in the subgrid
                else
                {
                    // top to bottom
                    FatherFaceIterator fatherIt(subGrid_,hostIterator_,FatherFaceIterator::begin);
                    FatherFaceIterator fatherEndIt(subGrid_,hostIterator_,FatherFaceIterator::end);

                    // store the inside element once and move it later
                    HostElement inside = hostIntersection.inside();

                    // get first internal face
                    while (true) {
                        if (fatherIt->inside().level() == 0 || (fatherIt->neighbor() && subGrid_->template contains<0>(fatherIt->outside())))
                            break;
                        else if (fatherIt == fatherEndIt) {
                        // if no father found the intersection is in the inside of an element that is not completly covered by the subgrid children,
                        // This can happen when "insertPartial" is used. In this case this is a boundary intersection
                            hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection), std::move(inside));
                            return;
                        }
                        else
                            ++fatherIt;
                    }

                    HostElement father = fatherIt->outside();

                    // if the bottom element is not leaf in the subgrid, then this element is partially
                    // covered and this intersection is at the boundary
                    if (!subGrid_->template isLeaf<0>(father))
                    {
                        // make boundary intersection iterator
                        hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection), std::move(inside));
                        return;
                    }

                    // flip the intersection to get the right IndexInOutside
                    HostLevelIntersectionIterator i = subGrid_->hostIIFactory().iLevelBegin(father);
                    while (i->boundary() || (!i->neighbor()) ||
                            (i->neighbor() && i->outside() != fatherIt->inside()))
                        ++i;

                    if (subGrid_->template contains<0>(father))
                        // make a singleton iterator containing only the intersection to the father element
                        hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection), std::move(inside), std::move(father), *i);
                    else
                        // make boundary intersection iterator, what about boundary ids?
                        hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection), std::move(inside));
                }
            }
            else
            {

                // store the inside element once and move it later
                HostElement inside = hostIntersection.inside();

                if (hostIntersection.boundary())
                {
                    // make boundary intersection
                    hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection),std::move(inside));
                }
                else
                {

                    // top to bottom
                    FatherFaceIterator fatherIt(subGrid_,hostIterator_,FatherFaceIterator::begin);
                    FatherFaceIterator fatherEndIt(subGrid_,hostIterator_,FatherFaceIterator::end);

                    // get first internal face
                    while (true) {
                        if (fatherIt->inside().level() == 0 || (fatherIt->neighbor() && subGrid_->template contains<0>(fatherIt->outside())))
                            break;
                        else if (fatherIt == fatherEndIt) {
                        // if no father found the intersection is in the inside of an element that is not completly covered by the subgrid children,
                        // This can happen when "insertPartial" is used. In this case this is a boundary intersection
                            hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection), std::move(inside));
                            return;
                        }
                        else
                            ++fatherIt;
                    }

                    HostElement father = fatherIt->outside();

                    // if the bottom element is not leaf in the subgrid, then this element is partially
                    // covered and this intersection is at the boundary
                    if (!subGrid_->template isLeaf<0>(father))
                    {
                        // make boundary intersection iterator
                        hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection), std::move(inside));
                        return;
                    }

                    // flip the intersection to get the right IndexInOutside
                    HostLevelIntersectionIterator i = subGrid_->hostIIFactory().iLevelBegin(father);
                    while (i->boundary() || (!i->neighbor()) ||
                            (i->neighbor() && i->outside() != fatherIt->inside()))
                        ++i;

                    if (subGrid_->template contains<0>(father))
                        hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection),std::move(inside), std::move(father), *i);
                    else // push_back boundary_intersection
                        hierarchicFaceIterator_ = HierarchicFaceIterator(subGrid_, std::move(hostIntersection), std::move(inside));
                }
            }
        }
    }

    //! equality
    bool equals(const SubGridLeafIntersectionIterator<GridImp>& other) const {
        return (hostIterator_ == other.hostIterator_ &&
                    hierarchicFaceIterator_ == other.hierarchicFaceIterator_);
    }

    //! prefix increment
    void increment() {
        ++hierarchicFaceIterator_;
        if(hierarchicFaceIterator_ == endFaceIterator_) {
            ++hostIterator_;
            initSubIntersections();
        }
    }

    //! \brief dereferencing
    const Intersection & dereference() const
    {
        return *hierarchicFaceIterator_;
    }

private:
    const GridImp* subGrid_;
    //! IntersectionIterator on the host grid
    HostLevelIntersectionIterator hostIterator_;
    HostLevelIntersectionIterator endIterator_;

    //! Hierarchic face iterator that iterate over the subintersection
    HierarchicFaceIterator hierarchicFaceIterator_;
    HierarchicFaceIterator endFaceIterator_;

};

}  // namespace Dune

#endif // DUNE_SUBGRID_SUBGRID_LEAF_INTERSECTION_ITERATOR_HH
