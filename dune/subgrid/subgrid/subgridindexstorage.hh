#ifndef DUNE_SUBGRID_INDEXSTORAGE_HH
#define DUNE_SUBGRID_INDEXSTORAGE_HH

/** \file
* \brief The SubGridIndexStorage classes
*/

#include <dune/common/hybridutilities.hh>
#include <dune/common/std/type_traits.hh>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/typeindex.hh>

#include <vector>
#include <map>
#include <set>
#include <iomanip>

namespace Dune {


// forward declarations
template <class GridType>
class SubGridMapIndexStorage;

template <class GridType>
class SubGridVectorIndexStorage;




//! contains a counter for each level and the leaf level
struct MultilevelCounter
{
    int leaf;
    std::vector<int> level;
};




//! Contains some tools to handle std::vector storage
template <typename T>
struct VectorTools
{
    //! \todo Please doc me !
    static void shrink(std::vector<T>& v)
    {
        std::vector<T>(v).swap(v);
        return;
    }


    //! \todo Please doc me !
    static void clearAndShrink(std::vector<T>& v)
    {
        std::vector<T>().swap(v);
        return;
    }
};




/** \brief Base class for index storage
*
* Contains the data structures and methods to handle:
* GeometryType, sizes, host indices, host ids
*/
template <class GridType>
class SubGridIndexStorageBase
{
    template <class GridImp_, int EntityDim>
    friend class SubGridMapSubindexSetter;

        using ctype = typename GridType::ctype;

    public:
        template <int codim>
        using GridEntity = typename std::remove_const<GridType>::type::template Codim<codim>::Entity;

        typedef typename std::remove_const<GridType>::type::HostGridType HostGridType;
        typedef typename HostGridType::Traits::GlobalIdSet HostIdSet;
        typedef typename HostIdSet::IdType HostIdType;

        //! type map GeometryTypes to MultilevelCounters
        typedef std::map<GeometryType, MultilevelCounter> EntityCounter;

        enum {dim = std::remove_const<GridType>::type::dimension};

        // The codimension of this level iterator wrt the host grid
        enum {CodimInHostGrid = HostGridType::dimension - dim};

        /** \brief Virtual constructor */
        virtual ~SubGridIndexStorageBase() {}

        //! This constructor initializes the grid and hostgrid references
        SubGridIndexStorageBase (const GridType& _grid) :
            grid(_grid),
            hostgrid(_grid.getHostGrid())
        {}


        //! get number of entities of given codim and and level
        int levelSize (int codim, int level) const
        {
            unsigned int sum = 0;
            for (const auto& type : levelTypes[dim-codim][level])
                sum += numEntities.find(type)->second.level[level];
            return sum;
        }


        //! get number of entities of given type and and level
        int levelSize (GeometryType type, int level) const
        {
            const auto it = numEntities.find(type);
            return (it==numEntities.end()) ? 0 : it->second.level[level];
        }


        //! deliver all geometry types used on given level
        const std::vector<GeometryType>& levelGeomTypes (int codim, int level) const
        {
            return levelTypes[dim-codim][level];
        }


        //! get number of entities of given codim and leaf level
        int leafSize (int codim) const
        {
            unsigned int sum = 0;
            for (const auto& type : leafTypes[dim-codim])
                sum += numEntities.find(type)->second.leaf;
            return sum;
        }


        //! get number of entities of given type and leaf level
        int leafSize (const GeometryType& type) const
        {
            const auto it = numEntities.find(type);
            return (it==numEntities.end()) ? 0 : it->second.leaf;
        }


        //! deliver all geometry types used on leaf level
        const std::vector<GeometryType>& leafGeomTypes (int codim) const
        {
            return leafTypes[dim-codim];
        }


        //! print information about stored indices
        virtual void report() const
        {}

    protected:

        //! construct vectors of GeometryTypes
        void rebuildGeometryTypes()
        {
            for (int i=0; i<dim+1; i++)
            {
                leafTypes[i].clear();
                levelTypes[i].clear();
                levelTypes[i].resize(grid.maxLevel()+1);
            }

            for (const auto& it : numEntities)
            {
                if (it.second.leaf > 0)
                    leafTypes[it.first.dim()].push_back(it.first);

                for (size_t level=0; level < it.second.level.size(); ++level)
                    if (it.second.level[level] > 0)
                        levelTypes[it.first.dim()][level].push_back(it.first);
            }
        }


        //! get multilevel geometry type counter for given geometry type for usage on given level
        MultilevelCounter& getGeometryTypeCountForLevel(const GeometryType& gt, int level DUNE_UNUSED)
        {
            // Is this the first time we see this kind of entity?
            auto countIt = numEntities.find(gt);
            if (countIt == numEntities.end())
            {
                MultilevelCounter newGeometryType;
                newGeometryType.leaf = 0;
                newGeometryType.level.resize(grid.maxLevel()+1, 0);

                auto p = numEntities.insert(std::make_pair(gt, newGeometryType));
                return p.first->second;
            }
            return countIt->second;
        }

        const MultilevelCounter& getGeometryTypeCountForLevel(const GeometryType& gt, int level DUNE_UNUSED) const
        {
            return numEntities.find(gt)->second;
        }


        //! get geometry type of given subentity
        template <int cc>
        GeometryType getSubGeometryType(const GridEntity<cc>& e, int i, unsigned int codim) const
        {
            return Dune::ReferenceElements<ctype,dim>::general(e.type()).type(i,codim);
        }


        //! get level index of host grid entity encapsulated in given entity
        template <int codim>
        int getHostLevelIndex(int level, const GridEntity<codim>& e) const
        {
            return hostgrid.levelIndexSet(level).index(grid.getRealImplementation(e).hostEntity());
        }


        //! get level index of host grid subentity encapsulated in given subentity
        template <int cc>
        int getHostLevelSubIndex(int level, const GridEntity<cc>& e, int i, unsigned int codim) const
        {
            return hostgrid.levelIndexSet(level).subIndex(grid.getRealImplementation(e).hostEntity(), i, codim+CodimInHostGrid);
        }


        //! get id of host grid entity encapsulated in given entity
        template <int codim>
        HostIdType getHostId(const GridEntity<codim>& e) const
        {
            return hostgrid.globalIdSet().id(grid.getRealImplementation(e).hostEntity());
        }


        //! get id of host grid subentity encapsulated in given subentity
        HostIdType getHostSubId(const GridEntity<0>& e, int i, unsigned int codim) const
        {
            return hostgrid.globalIdSet().subId(grid.getRealImplementation(e).hostEntity(), i, codim);
        }


        const GridType& grid;
        const HostGridType& hostgrid;

        //! stores a MultilevelCounter for each GeometryType
        EntityCounter numEntities;

        //! stores a vector of present GeometryTypes for each codim and level
        std::vector< std::vector<GeometryType> > levelTypes[dim+1];

        //! stores a vector of present GeometryTypes for each codim on leaf level
        std::vector<GeometryType> leafTypes[dim+1];
};




// *********************************************************************************************************************
// classes needed by map based storage engine
// *********************************************************************************************************************

//! \todo Please doc me !
class SubGridMultilevelIndex
{
    public:

        //! \todo Please doc me !
        SubGridMultilevelIndex ()
        {
            leaf = -1;

            minLevel = 255;
            maxLevel = 255;

            level.index = -1;
        }


        //! \todo Please doc me !
        SubGridMultilevelIndex(const SubGridMultilevelIndex& original)
        {
            leaf = original.leaf;

            minLevel = original.minLevel;
            maxLevel = original.maxLevel;

            if (minLevel < maxLevel)
            {
                int levels = maxLevel - minLevel + 1;
                level.indices = new int[levels];
                for(int i=0; i<levels; ++i)
                    level.indices[i] = original.level.indices[i];
            }
            else
                level.index = original.level.index;
        }


        //! \todo Please doc me !
        void prepareIfNew(int _level, int dim)
        {
            if (minLevel==255)
            {
                leaf = -1;

                maxLevel = _level;

                if (dim != 0)
                    minLevel = _level;
                else
                    minLevel = 0;

                if (minLevel<maxLevel)
                {
                    int levels = maxLevel - minLevel + 1;
                    level.indices = new int[levels];
                    for(int i=0; i<levels; ++i)
                        level.indices[i] = -1;
                }
                else
                    level.index = -1;
            }
            return;
        }


        //! \todo Please doc me !
        ~SubGridMultilevelIndex()
        {
            if (minLevel < maxLevel)
                delete[] level.indices;
        }


        //! \todo Please doc me !
        SubGridMultilevelIndex& operator=(const SubGridMultilevelIndex& original)
        {
            if (this != &original)
            {
                if (minLevel < maxLevel)
                    delete[] level.indices;

                leaf = original.leaf;

                minLevel = original.minLevel;
                maxLevel = original.maxLevel;

                if (minLevel < maxLevel)
                {
                    int levels = maxLevel - minLevel + 1;
                    level.indices = new int[levels];
                    for(int i=0; i<levels; ++i)
                        level.indices[i] = original.level.indices[i];
                }
                else
                    level.index = original.level.index;
            }
            return *this;
        }


        //! \todo Please doc me !
        void setLevelIndex(int _level, int _index)
        {
            if (minLevel>_level)
            {
                int levels = maxLevel - _level + 1;
                int* newLevelIndices = new int[levels];
                for(int i=0; i<levels; ++i)
                    newLevelIndices[i] = -1;
                int levelDiff = minLevel - _level;

                if (minLevel<maxLevel)
                {
                    levels = maxLevel - minLevel + 1;
                    for(int i=0; i<levels; ++i)
                        newLevelIndices[i+levelDiff] = level.indices[i];
                    delete[] level.indices;
                    level.indices = newLevelIndices;
                }
                else
                    newLevelIndices[levelDiff] = level.index;
                minLevel = _level;
            }
            else if (_level>maxLevel)
            {
                int levels = _level - minLevel + 1;
                int* newLevelIndices = new int[levels];
                for(int i=0; i<levels; ++i)
                    newLevelIndices[i] = -1;
                if (minLevel<maxLevel)
                {
                    levels = maxLevel - minLevel + 1;
                    for(int i=0; i<levels; ++i)
                        newLevelIndices[i] = level.indices[i];
                    delete[] level.indices;
                    level.indices = newLevelIndices;
                }
                else
                    newLevelIndices[0] = level.index;
                maxLevel = _level;
            }
            if (minLevel<maxLevel)
                level.indices[_level-minLevel] = _index;
            else
                level.index = _index;
            return;
        }


        //! \todo Please doc me !
        int getLevelIndex(int _level) const
        {
            if (minLevel>_level)
                return -1;
            else if (_level>maxLevel)
                return -1;
            if (minLevel<maxLevel)
                return level.indices[_level-minLevel];
            return level.index;
        }


        //! \todo Please doc me !
        bool levelIndexNotSet(int _level) const
        {
            return (getLevelIndex(_level)<0);
        }


        //! \todo Please doc me !
        bool leafIndexNotSet() const
        {
            return (leaf<0);
        }


        //! \todo Please doc me !
        bool isLeafOnLevel(int _level) const
        {
            if (leaf<0)
                return false;
            if (_level<maxLevel)
                return false;
            return true;
        }


        //! \todo Please doc me !
        int leaf;


    private:

        //! \todo Please doc me !
        unsigned char minLevel;

        //! \todo Please doc me !
        unsigned char maxLevel;


        //! \todo Please doc me !
        union
        {
            int index;
            int* indices;
        } level;
};




//! helper class with template meta program to set indices for codim>0 subentities
template <class GridType, int codim>
class SubGridMapSubindexSetter
{
    public:

        typedef typename std::remove_const<GridType>::type::HostGridType HostGridType;
        typedef typename HostGridType::Traits::GlobalIdSet HostIdSet;
        typedef typename HostIdSet::IdType HostIdType;

        typedef typename std::remove_const<GridType>::type::template Codim<0>::Entity Element;

        enum {dim = std::remove_const<GridType>::type::dimension};


        //! \todo Please doc me !
        static void insertSubEntities(SubGridMapIndexStorage<GridType>& indexStorage, const Element& e, bool isLeaf, int level)
        {
            for(size_t i=0; i < e.subEntities(codim); ++i)
            {
                // get index storage of entity
                HostIdType id = indexStorage.getHostSubId(e, i, codim);
                GeometryType gt = indexStorage.template getSubGeometryType<0>(e, i, codim);
                SubGridMultilevelIndex &index = indexStorage.indices[codim][id];

                // if index is newly created prepare it for use with level and codim
                index.prepareIfNew(level, dim-codim);

                // if level index is not set use next level index
                if (index.levelIndexNotSet(level))
                {
                    // get counter for this GeometryType
                    MultilevelCounter &count = indexStorage.getGeometryTypeCountForLevel(gt, level);

                    // set level index
                    index.setLevelIndex(level, (count.level[level])++);
                }

                // if entity is leaf and leaf index is not set use next leaf index
                if (isLeaf)
                {
                    if (index.leafIndexNotSet())
                    {
                        // get counter for this GeometryType
                        MultilevelCounter &count = indexStorage.getGeometryTypeCountForLevel(gt, level);

                        // set level index
                        index.leaf = count.leaf++;
                    }
                }
            }

            // recursive call of template meta program to set indices for codim-1 subentities
            SubGridMapSubindexSetter<GridType, codim-1>::insertSubEntities(indexStorage, e, isLeaf, level);

            return;
        }
};

#ifdef SUBGRID_NO_FACES
// skip faces if needed
template <class GridType>
class SubGridMapSubindexSetter<GridType,2>
{
    public:

        typedef typename std::remove_const<GridType>::type::template Codim<0>::Entity Element;

        static void insertSubEntities(SubGridMapIndexStorage<GridType>& indexStorage, const Element& e, bool isLeaf, int level)
        {
            SubGridMapSubindexSetter<GridType, 1>::insertSubEntities(indexStorage, e, isLeaf, level);
        };
};
#endif

#ifdef SUBGRID_NO_EDGES
// skip edges if needed
template <class GridType>
class SubGridMapSubindexSetter<GridType,1>
{
    public:

        typedef typename std::remove_const<GridType>::type::template Codim<0>::Entity Element;

        static void insertSubEntities(SubGridMapIndexStorage<GridType>& indexStorage, const Element& e, bool isLeaf, int level)
        {
            SubGridMapSubindexSetter<GridType, 0>::insertSubEntities(indexStorage, e, isLeaf, level);
        };
};
#endif

// end of template meta program recursion
template <class GridType>
class SubGridMapSubindexSetter<GridType,0>
{
    public:

        typedef typename std::remove_const<GridType>::type::template Codim<0>::Entity Element;

        static void insertSubEntities(SubGridMapIndexStorage<GridType>& indexStorage DUNE_UNUSED, const Element& e DUNE_UNUSED, bool isLeaf DUNE_UNUSED, int level DUNE_UNUSED)
        {};
};




//! \todo Please doc me !
template <class GridType>
class SubGridMapIndexStorage :
    public SubGridIndexStorageBase<GridType>
{
        template <class GridImp_, int EntityDim>
        friend class SubGridMapSubindexSetter;

        using Base = SubGridIndexStorageBase<GridType>;

        template <int codim>
        using GridEntity = typename Base::template GridEntity<codim>;
    public:

        using HostGridType = typename Base::HostGridType;
        using HostIdSet = typename Base::HostIdSet;
        using HostIdType = typename Base::HostIdType;

        //! \todo Please doc me !
        typedef typename std::map<HostIdType, SubGridMultilevelIndex> GlobalToIndexMap;

        enum {dim = std::remove_const<GridType>::type::dimension};


        //! \todo Please doc me !
        SubGridMapIndexStorage (const GridType& _grid) :
            Base(_grid)
        {}


        //! \todo Please doc me !
        void update()
        {
            for (int i=0; i<dim+1; ++i)
                indices[i].clear();

            numEntities.clear();

            std::set<HostIdType> fatherBuffer;

            HostIdType id;
            HostIdType fatherId;

            for (int level=grid.maxLevel(); level>=0; --level)
            {
                for (const auto& it : elements(grid.levelGridView(level)))
                {
                    id = grid.globalIdSet().template id<0>(it);

                    if (fatherBuffer.find(id)==fatherBuffer.end())
                        insert(it, true, level);
                    else
                        insert(it, false, level);
                    if (level>0)
                    {
                        fatherId = grid.globalIdSet().template id<0>(it.father());
                        fatherBuffer.insert(fatherId);
                    }
                }
            }
            this->rebuildGeometryTypes();
        }


        //! \todo Please doc me !
        template <int codim>
        bool isLeaf(const GridEntity<codim>& e) const
        {
            HostIdType id = grid.globalIdSet().id(e);
            const auto it = indices[codim].find(id);
            if (it == indices[codim].end())
                return false;
            return it->second.isLeafOnLevel(e.level());
        }


        //! \todo Please doc me !
        template <int codim>
        bool isLeaf(const typename HostGridType::template Codim<codim>::Entity& e) const
        {
            HostIdType id = hostgrid.globalIdSet().id(e);
            const auto it = indices[codim].find(id);
            if (it == indices[codim].end())
                return false;
            return it->second.isLeafOnLevel(e.level());
        }


        //! \todo Please doc me !
        template <int codim>
        int levelIndex(const GridEntity<codim>& e, int level) const
        {
            HostIdType id = grid.globalIdSet().id(e);
            const auto it = indices[codim].find(id);
            if (it == indices[codim].end())
                return -1;
            return it->second.getLevelIndex(level);
        }


        //! \todo Please doc me !
        template <int cc>
        int levelSubIndex(const GridEntity<cc>& e, int i, int level, unsigned int codim) const
        {
            int result = -1;
            Hybrid::ifElse(Std::bool_constant<cc==0>(),[&](auto id)
            {
                HostIdType hid = this->grid.globalIdSet().subId(e, i, codim);
                const auto it = indices[codim].find(hid);
                if (it != indices[codim].end())
                    result = it->second.getLevelIndex(level);
            });

            Hybrid::ifElse(Std::bool_constant<cc!=0>(),[&](auto id)
            {
                DUNE_THROW( NotImplemented, "subIndex for higher codimension entity not implemented for SubGrid." );
            });
            return result;
        }


        //! \todo Please doc me !
        template <int codim>
        int leafIndex(const GridEntity<codim>& e) const
        {
            HostIdType id = grid.globalIdSet().id(e);
            const auto it = indices[codim].find(id);
            if (it == indices[codim].end())
                return -1;
            return it->second.leaf;
        }


        //! \todo Please doc me !
        template <int cc>
        int leafSubIndex(const GridEntity<cc>& e, int i, unsigned int codim) const
        {
            int result = -1;
            Hybrid::ifElse(Std::bool_constant<cc==0>(),[&](auto id)
            {
                HostIdType hid = this->grid.globalIdSet().subId(e, i, codim);
                const auto it = indices[codim].find(hid);
                if (it == indices[codim].end())
                    result = it->second.leaf;
            });

            Hybrid::ifElse(Std::bool_constant<cc!=0>(),[&](auto id)
            {
                DUNE_THROW( NotImplemented, "subIndex for higher codimension entity not implemented for SubGrid." );
            });
            return result;
        }


    private:

        // this should only be called by update
        //! \todo Please doc me !
        void insert(const GridEntity<0>& e, bool isLeaf, int level)
        {
            // get index storage of entity
            HostIdType id = this->template getHostId<0>(e);
            GeometryType gt = e.type();
            SubGridMultilevelIndex &index = indices[0][id];

            // if index is newly created prepare it for use with level and codim
            index.prepareIfNew(level, dim);

            // get counter for this GeometryType
            MultilevelCounter &count = this->getGeometryTypeCountForLevel(gt, level);

            // set level and leaf indices
            index.setLevelIndex(level, (count.level[level])++);
            if (isLeaf)
                index.leaf = count.leaf++;

            // call of template meta program to set indices for codim>0 subentities
            SubGridMapSubindexSetter<GridType,dim>::insertSubEntities(*this, e, isLeaf, level);
        }


    protected:

        using Base::grid;
        using Base::hostgrid;
        using Base::numEntities;
        using Base::levelTypes;
        using Base::leafTypes;

        //data members, protected since helper class SubGridMapSubindexSetter need access

        //! \todo Please doc me !
        GlobalToIndexMap indices[dim+1];
};




// *********************************************************************************************************************
// classes needed by map vector storage engine
// *********************************************************************************************************************

//! \todo Please doc me !
class LeafOnLevelCounter
{
    public:

        //! \todo Please doc me !
        LeafOnLevelCounter ()
        {
            leafCount = 0;
            copyLeafCount = 0;
            newLeafCount = 0;

            newLeafIndexStart = 0;
            newLeafOffset = 0;
        }


        //! Total number of leaf entities
        int leafCount;

        //! Number of copied leaf entities
        int copyLeafCount;

        //! Number of leaf entities that are no copy of coarser level entities
        int newLeafCount;

        //! Leaf index of first leaf entity that is not a copy on that level
        int newLeafIndexStart;

        //! The shift that is used to compute leaf indices of new leaf entities from the level index of the leaf entity
        int newLeafOffset;
};




// helper class with template meta program to set indices for codim>0 subentities
template <class GridType, int codim>
class SubGridVectorSubindexSetter
{
    public:

        typedef typename std::remove_const<GridType>::type::HostGridType HostGridType;
        typedef typename HostGridType::Traits::GlobalIdSet HostIdSet;
        typedef typename HostIdSet::IdType HostIdType;

        typedef typename std::remove_const<GridType>::type::template Codim<0>::Entity Element;

        enum {dim = std::remove_const<GridType>::type::dimension};


        //! \todo Please doc me !
        static void insertSubEntities(SubGridVectorIndexStorage<GridType>& indexStorage, const Element& e, bool isLeaf, int level)
        {
            for(uint i=0; i < e.subEntities(codim); ++i)
            {
                // get host index and GeometryType index of entity
                int hostIndex = indexStorage.template getHostLevelSubIndex<0>(level, e, i, codim);
                GeometryType gt = indexStorage.template getSubGeometryType<0>(e, i, codim);
                int gtIndex = GlobalGeometryTypeIndex::index(gt);

                int &entityLevelIndex = indexStorage.levelIndices[level][gtIndex][hostIndex];

                // if level index is already set skip this entity
                if (indexStorage.indexUninitializedOrVisited(entityLevelIndex))
                {
                    if (isLeaf)
                    {
                        // if subentity is on level > 0
                        //     check if it's a copy of a leaf entity on coarser level
                        if (level>0)
                        {
                            // as long as there are nonleaf copies
                            // there might be a leaf copy on a coarser level
                            HostIdType id = indexStorage.getHostSubId(e, i, codim);
                            Element father(e);
                            int fatherLevel = level;
                            bool nonLeafCopyFound = true;
                            while (nonLeafCopyFound)
                            {
                                nonLeafCopyFound = false;
                                father = father.father();
                                --fatherLevel;
                                for(uint j=0; j < father.subEntities(codim); ++j)
                                {
                                    // if subentity is copy of ancestor subentity
                                    //     copy leaf index from father subentity
                                    //     mark father subentities as nonleaf
                                    //     mark subentity as leaf
                                    if (indexStorage.getHostSubId(father, j, codim) == id)
                                    {

                                        int fatherLeafIndex = indexStorage.template leafSubIndex<0>(father, j, codim);
                                        if (fatherLeafIndex < 0)
                                            nonLeafCopyFound = true;
                                        else
                                        {
                                            entityLevelIndex = indexStorage.leafInfo[level][gtIndex].copyLeafCount;
                                            indexStorage.leafIndices[level][gtIndex][entityLevelIndex] = fatherLeafIndex;

                                            int fatherLevelIndex = indexStorage.template levelSubIndex<0>(father, j, fatherLevel, codim);
                                            indexStorage.leafState[fatherLevel][gtIndex][fatherLevelIndex] = false;

                                            ++indexStorage.leafInfo[level][gtIndex].copyLeafCount;
                                        }

                                        break;
                                    }
                                }
                                if (father.level()==0)
                                    break;
                            }
                        }
                        // if subentity was not found as copy on coarser level
                        //     treat it as new leaf subentity
                        if (indexStorage.indexUninitializedOrVisited(entityLevelIndex))
                        {
                            entityLevelIndex = indexStorage.hasLeafIndexValue;
                            ++indexStorage.leafInfo[level][gtIndex].newLeafCount;
                        }
                    }
                    else
                        entityLevelIndex = indexStorage.visitedValue;
                }
            }

            // recursive call of template meta program to set indices for codim-1 subentities
            SubGridVectorSubindexSetter<GridType, codim-1>::insertSubEntities(indexStorage, e, isLeaf, level);
        }
};

#ifdef SUBGRID_NO_FACES
// skip faces if needed
template <class GridType>
class SubGridVectorSubindexSetter<GridType,2>
{
    public:

        typedef typename std::remove_const<GridType>::type::template Codim<0>::Entity Element;

        static void insertSubEntities(SubGridVectorIndexStorage<GridType>& indexStorage, const Element& e, bool isLeaf, int level)
        {
            SubGridVectorSubindexSetter<GridType, 1>::insertSubEntities(indexStorage, e, isLeaf, level);
        };
};
#endif

#ifdef SUBGRID_NO_EDGES
// skip edges if needed
template <class GridType>
class SubGridVectorSubindexSetter<GridType,1>
{
    public:

        typedef typename std::remove_const<GridType>::type::template Codim<0>::Entity Element;

        static void insertSubEntities(SubGridVectorIndexStorage<GridType>& indexStorage, const Element& e, bool isLeaf, int level)
        {
            SubGridVectorSubindexSetter<GridType, 0>::insertSubEntities(indexStorage, e, isLeaf, level);
        };
};
#endif

// end of template meta program recursion
template <class GridType>
class SubGridVectorSubindexSetter<GridType,0>
{
    public:

        typedef typename std::remove_const<GridType>::type::template Codim<0>::Entity Element;

        static void insertSubEntities(SubGridVectorIndexStorage<GridType>& indexStorage DUNE_UNUSED, const Element& e DUNE_UNUSED, bool isLeaf DUNE_UNUSED, int level DUNE_UNUSED)
        {};
};




//! \todo Please doc me !
template <class GridType>
class SubGridVectorIndexStorage :
    public SubGridIndexStorageBase<GridType>
{
        template <class GridImp_, int EntityDim>
        friend class SubGridVectorSubindexSetter;

        using Base = SubGridIndexStorageBase<GridType>;

        template <int codim>
        using GridEntity = typename Base::template GridEntity<codim>;
    public:

        using HostGridType = typename Base::HostGridType;
        using HostIdSet = typename Base::HostIdSet;
        using HostIdType = typename Base::HostIdType;

        //! For all possible geometry types of a grid dimension, store an index for each entity in the host grid
        typedef typename std::vector<std::vector<int> > EntityToIndex;

        //! \todo Please doc me !
        typedef typename std::vector<std::vector<bool> > EntityToBit;

        //! \todo Please doc me !
        typedef typename std::vector<EntityToIndex> MultilevelEntityToIndex;

        //! \todo Please doc me !
        typedef typename std::vector<EntityToBit> MultilevelEntityToBit;

        enum {dim = std::remove_const<GridType>::type::dimension};


        //! \todo Please doc me !
        SubGridVectorIndexStorage (const GridType& _grid) :
            Base(_grid)
        {}

        bool indexUninitializedOrVisited(int index) const
        {
            return (index<-1);
        }

        bool indexContained(int index) const
        {
            return (index>-3);
        }

        //! Setup the index storage
        void update()
        {
            numEntities.clear();

            levelIndices.clear();
            leafInfo.clear();
            leafIndices.clear();
            leafState.clear();

            levelIndices.resize(grid.maxLevel()+1);
            leafInfo.resize(grid.maxLevel()+1);
            leafIndices.resize(grid.maxLevel()+1);
            leafState.resize(grid.maxLevel()+1);

            for (int level=0; level<=grid.maxLevel(); ++level)
            {
                levelIndices[level].resize(GlobalGeometryTypeIndex::size(dim));
                leafInfo[level].resize(GlobalGeometryTypeIndex::size(dim));
                leafIndices[level].resize(GlobalGeometryTypeIndex::size(dim));
                leafState[level].resize(GlobalGeometryTypeIndex::size(dim));

                for (int codim=0; codim<=dim; ++codim)
                {
                    const auto& types = hostgrid.levelIndexSet(level).types(codim);
                    for (const auto& type : types)
                    {
                        int gtIndex = GlobalGeometryTypeIndex::index(type);

                        levelIndices[level][gtIndex].resize(hostgrid.size(level, type), uninitializedValue);
                        leafIndices[level][gtIndex].resize(hostgrid.size(level, type), uninitializedValue);
                    }
                }

                // insert elements
                const auto& levelGridView = grid.levelGridView(level);
                if (level==grid.maxLevel())
                {
                    for(const auto& element  : elements(levelGridView))
                        insert(element, true, level);
                }
                else
                {
                    for(const auto& element: elements(levelGridView))
                    {
                        if (element.hbegin(level+1)==element.hend(level+1))
                            insert(element, true, level);
                        else
                            insert(element, false, level);
                    }
                }

                // now we build the rest of the real level indices
                for (int codim=0; codim<=dim; ++codim)
                {
                    for (const auto& gt : hostgrid.levelIndexSet(level).types(codim))
                    {
                        int gtIndex = GlobalGeometryTypeIndex::index(gt);

                        MultilevelCounter &count = this->getGeometryTypeCountForLevel(gt, level);

                        LeafOnLevelCounter& counter = leafInfo[level][gtIndex];

                        if (level>0)
                        {
                            LeafOnLevelCounter& previousLevel = leafInfo[level-1][gtIndex];
                            counter.newLeafIndexStart = previousLevel.newLeafIndexStart + previousLevel.newLeafCount;
                        }
                        else
                            counter.newLeafIndexStart = 0;

                        counter.leafCount = counter.copyLeafCount + counter.newLeafCount;
                        counter.newLeafOffset = counter.newLeafIndexStart - counter.copyLeafCount;


                        leafIndices[level][gtIndex].resize(counter.copyLeafCount);
                        VectorTools<int>::shrink(leafIndices[level][gtIndex]);

                        leafState[level][gtIndex].clear();
                        VectorTools<bool>::shrink(leafState[level][gtIndex]);
                        leafState[level][gtIndex].resize(counter.leafCount, true);

                        std::vector<int>& gtLevelIndices = levelIndices[level][gtIndex];

                        // new leaf indices start after the copy leaf indices
                        int newLeafCounter = counter.copyLeafCount;
                        // the non-leaf indices start after the new leaf indices
                        int nonLeafCounter = counter.leafCount;
                        // copy leaf indices were already set in the helper function
                        for (auto& gtLevelIndex : gtLevelIndices)
                        {
                            if (indexContained(gtLevelIndex))
                            {
                                ++count.level[level];
                                if (gtLevelIndex == visitedValue)
                                    gtLevelIndex = nonLeafCounter++;
                                else if (gtLevelIndex == hasLeafIndexValue)
                                {
                                    gtLevelIndex = newLeafCounter++;
                                    ++count.leaf;
                                }
                            }
                        }
                    }
                }
            }
            this->rebuildGeometryTypes();
        }


        //! \todo Please doc me !
        template <int codim>
        bool isLeaf(const typename std::remove_const<HostGridType>::type::template Codim<codim>::Entity& e) const
        {
            int level = e.level();
            int hostIndex = hostgrid.levelIndexSet(level).index(e);

            int gtIndex = GlobalGeometryTypeIndex::index(e.type());
            int subGridLevelIndex = levelIndices[level][gtIndex][hostIndex];

            if (not(indexContained(subGridLevelIndex)))
                return false;

            const LeafOnLevelCounter& counter = leafInfo[level][gtIndex];
            if (subGridLevelIndex >= counter.leafCount)
                return false;
            return leafState[level][gtIndex][subGridLevelIndex];
        }


        //! \todo Please doc me !
        template <int codim>
        int levelIndex(const GridEntity<codim>& e, int level) const
        {
            return levelIndices[level][GlobalGeometryTypeIndex::index(e.type())][this->template getHostLevelIndex<codim>(level, e)];
        }


        //! \todo Please doc me !
        template <int cc>
        int levelSubIndex(const GridEntity<cc>& e, int i, int level, unsigned int codim) const
        {
            if (cc!=0)
                DUNE_THROW( NotImplemented, "levelSubIndex for higher codimension entity not implemented for SubGrid." );
            return levelIndices[level][GlobalGeometryTypeIndex::index(Base::template getSubGeometryType<cc>(e, i, codim))][Base::template getHostLevelSubIndex<cc>(level, e, i, codim)];
        }


        //! \todo Please doc me !
        template <int codim>
        int leafIndex(const GridEntity<codim>& e) const
        {
            int level = e.level();
            int gtIndex = GlobalGeometryTypeIndex::index(e.type());
            int subGridLevelIndex = levelIndices[level][gtIndex][this->template getHostLevelIndex<codim>(level, e)];

            const LeafOnLevelCounter& counter = leafInfo[level][gtIndex];
            if (subGridLevelIndex < counter.copyLeafCount)
                return leafIndices[level][gtIndex][subGridLevelIndex];
            if (subGridLevelIndex < counter.leafCount)
                return subGridLevelIndex+counter.newLeafOffset;
            return -1;
        }


        //! \todo Please doc me !
        template <int cc>
        int leafSubIndex(const GridEntity<cc>& e, int i, unsigned int codim) const
        {
            if (cc!=0)
                DUNE_THROW( NotImplemented, "leafSubIndex for higher codimension entity not implemented for SubGrid." );
            int level = e.level();
            int gtIndex = GlobalGeometryTypeIndex::index(Base::template getSubGeometryType<cc>(e, i, codim));
            int subGridLevelIndex = levelIndices[level][gtIndex][Base::template getHostLevelSubIndex<cc>(level, e, i, codim)];

            const LeafOnLevelCounter& counter = leafInfo[level][gtIndex];
            if (subGridLevelIndex < counter.copyLeafCount)
                return leafIndices[level][gtIndex][subGridLevelIndex];
            if (subGridLevelIndex < counter.leafCount)
                return subGridLevelIndex+counter.newLeafOffset;
            return -1;
        }


        //! \todo Please doc me !
        void report(int level, const GeometryType& gt) const
        {
            int gtIndex = GlobalGeometryTypeIndex::index(gt);
            const LeafOnLevelCounter& counter = leafInfo[level][gtIndex];

            std::cout << std::setw(2) << level << " " << std::setw(12) << gt;
            std::cout << std::setw(8) << this->getGeometryTypeCountForLevel(gt, level).level[level] << "/" << std::setw(8) << levelIndices[level][gtIndex].size();
            std::cout << " ~ " << std::setw(3) << ((this->getGeometryTypeCountForLevel(gt, level).level[level] * 100) / levelIndices[level][gtIndex].size()) << "%";
            std::cout << " | leaf:" << std::setw(8) << counter.copyLeafCount << " copies " << std::setw(8) << counter.newLeafCount << " new |";
            std::cout << " start:" << std::setw(8) << counter.newLeafIndexStart << " | offset:" << std::setw(8) << counter.newLeafOffset;
            std::cout << std::endl;
            return;
        }


        //! \todo Please doc me !
        void report() const
        {
            std::cout << std::setfill('-') << std::setw(112) << "" << std::setfill(' ') << std::endl;
            for (int level=0; level<=grid.maxLevel(); ++level)
            {
                for (int codim=0; codim<=dim; ++codim)
                    for (const auto& type : hostgrid.levelIndexSet(level).types(codim))
                        report(level, type);
                std::cout << std::setfill('-') << std::setw(112) << "" << std::setfill(' ') << std::endl;
            }
            return;
        }

    private:

        // this should only be called by update
        //! \todo Please doc me !
        void insert(const GridEntity<0>& e, bool isLeaf, int level)
        {
            // get host index and GeometryType index of entity
            int hostIndex = this->template getHostLevelIndex<0>(level, e);
            GeometryType gt = e.type();
            int gtIndex = GlobalGeometryTypeIndex::index(gt);

            if (isLeaf)
            {
                levelIndices[level][gtIndex][hostIndex] = hasLeafIndexValue;
                ++leafInfo[level][gtIndex].newLeafCount;
            }
            else
                levelIndices[level][gtIndex][hostIndex] = visitedValue;

            // call of template meta program to set indices for codim>0 subentities
            SubGridVectorSubindexSetter<GridType,dim>::insertSubEntities(*this, e, isLeaf, level);
        }


    protected:

        using Base::grid;
        using Base::hostgrid;
        using Base::numEntities;
        using Base::levelTypes;
        using Base::leafTypes;

        //data members, protected since helper class SubGridVectorSubindexSetter need access

        //! Value to mark that an index is still uninitialised
        int uninitializedValue{-3};

        //! Value to mark that an index has already been visited during the setup process
        int visitedValue{-2};

        //! Value to mark entities in the levelIndices vector which are leaf
        int hasLeafIndexValue{-1};

        //! For each level and each Geometry type this contains information about new leaf entities and copies of coarser ones
        std::vector< std::vector<LeafOnLevelCounter> > leafInfo;

        //! Contains leaf indices of leaf entities that are copies
        MultilevelEntityToIndex leafIndices;

        //! \todo Please doc me !
        MultilevelEntityToBit leafState;

        //! \todo Please doc me !
        MultilevelEntityToIndex levelIndices;
};


} // end namespace dune

#endif
