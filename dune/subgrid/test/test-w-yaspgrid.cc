#include <config.h>

#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/test/gridcheck.hh>
#include <dune/grid/test/checkintersectionit.hh>

#include <dune/subgrid/subgrid.hh>
#include "common.hh"

using namespace Dune;

// test subgrid for given dimension
template <int dim>
size_t testDim()
{
    typedef YaspGrid<dim> GridType;

    Dune::FieldVector<typename GridType::ctype,dim> L(1.0);
    std::array<int,dim> s;
    std::fill(s.begin(), s.end(), 1);
    std::bitset<dim> periodic(false);

    GridType grid(L,s,periodic,0);

    const int refine = 1;
    for (size_t i=0; i<refine; ++i)
        grid.globalRefine(1);

    SubGrid<dim,GridType> subGrid(grid);

    // create subgrid
#if 0
    {
        subGrid.createBegin();

        typename GridType::LevelGridView view = grid.levelGridView(0);
        for (const auto& e : elements(view)) {
            if (e.geometry().corner(0)[0] < 0.5)
                subGrid.insertRaw(e);
        }

        subGrid.createEnd();
    }
#endif
    {
        subGrid.createBegin();
        subGrid.insertLevel(grid.maxLevel());
        subGrid.createEnd();
        subGrid.setMaxLevelDifference(1);
    }

    subGrid.report();

    // check subgrid
    std::cout << "Check subgrid with dim=" << dim << std::endl;

    try {
        gridcheck(subGrid);
        checkIntersectionIterator(subGrid);
        checkLeafIntersections(subGrid);
    } catch(Dune::NotImplemented e) {
        std::cout<<e<<std::endl;
    }

    // refine subgrid
    {
        typename SubGrid<dim,GridType>::LevelGridView subView = subGrid.levelGridView(0);
        for (const auto& e : elements(subView)) {
            if (e.geometry().corner(0)[0] < 0.25)
                subGrid.mark(1, e);
        }

        subGrid.preAdapt();
        subGrid.adapt();
        subGrid.postAdapt();
    }

    // check locally refined subgrid
    std::cout << "Check locally refined subgrid with dim=" << dim << std::endl;

    try {
        gridcheck(subGrid);
        checkIntersectionIterator(subGrid);
        checkLeafIntersections(subGrid);
    } catch(Dune::NotImplemented e) {
        std::cout<<e<<std::endl;
    }
    return 0;
}


int main (int argc, char *argv[]) try
{
    Dune::MPIHelper::instance(argc, argv);

    size_t return_val = 0;
//     testDim<1>();
    return_val = std::max(return_val,testDim<2>());
    return_val = std::max(return_val,testDim<3>());

    std::cout << "Test for SubGrid with YaspGrid " << (return_val==0 ? "passed." : "failed.") << std::endl;
    return return_val;
} catch (Exception e) {

    std::cout << e << std::endl;
    return 1;
 }
