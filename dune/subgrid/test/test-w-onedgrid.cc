#include <config.h>

#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <dune/grid/onedgrid.hh>
#include <dune/grid/test/gridcheck.hh>
#include <dune/grid/test/checkintersectionit.hh>

#include <dune/subgrid/subgrid.hh>

using namespace Dune;

int main (int argc, char *argv[]) try
{
    typedef OneDGrid GridType;

    GridType grid(2,0.0,1.0);

    SubGrid<1,GridType> subGrid(grid);

    // create subgrid
    {
        subGrid.createBegin();

        typename GridType::LevelGridView view = grid.levelGridView(0);
        for (const auto& e : elements(view)) {
            if (e.geometry().corner(0)[0] < 0.5)
                subGrid.insertRaw(e);
        }

        subGrid.createEnd();
    }
    subGrid.report();

    // check subgrid
    std::cout << "Check subgrid<onedgrid>" << std::endl;

    try {
        gridcheck(subGrid);
        checkIntersectionIterator(subGrid);
    } catch(Dune::NotImplemented e) {
        std::cout<<e<<std::endl;
    }


    // refine subgrid
    {
        typename SubGrid<1,GridType>::LevelGridView subView = subGrid.levelGridView(0);
        for (const auto& e : elements(subView)) {
            if (e.geometry().corner(0)[0] < 0.25)
                subGrid.mark(1, e);
        }

        subGrid.preAdapt();
        subGrid.adapt();
        subGrid.postAdapt();
    }

    // check locally refined subgrid
    std::cout << "Check locally refined subgrid<onedgrid>" << std::endl;

    try{
        gridcheck(subGrid);
        checkIntersectionIterator(subGrid);
    } catch(Dune::NotImplemented e) {
        std::cout<<e<<std::endl;
    }
    return 0;
}

// //////////////////////////////////
//   Error handler
// /////////////////////////////////
catch (Exception e) {
    std::cout << e << std::endl;
    return 1;
}
