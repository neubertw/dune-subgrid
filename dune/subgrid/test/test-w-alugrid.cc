#include <config.h>

#include <dune/common/parallel/mpihelper.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh> // not included by alugrid itself
#endif

#include <dune/subgrid/test/common.hh>

int main(int argc, char* argv[]) try {
  Dune::MPIHelper::instance(argc, argv);

#if HAVE_DUNE_ALUGRID

  size_t return_val = 0;

  // 2D
  { // simplex nonconforming
     using Grid = Dune::ALUGrid<2, 2, Dune::simplex, Dune::nonconforming>;
     return_val = std::max(return_val, checkWithHostGrid<Grid, SIMPLEX>());
  }
  { // simplex conforming
    // using Grid = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>;
    // return_val = std::max(return_val, checkWithHostGrid<Grid, SIMPLEX>());
    std::cout << "Test for SubGrid<ALUGrid<2, 2, simplex, conforming>> is"
                 "currently disabled, due to a possible bug in dune-alugrid,"
                 "see "
                 "https://gitlab.dune-project.org/extensions/dune-alugrid/"
                 "issues/31" << std::endl;
  }
  { // cube nonconforming
     using Grid = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>;
     return_val = std::max(return_val, checkWithHostGrid<Grid, CUBE>());
  }

  // 3D
  { // simplex nonconforming
    // using Grid = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>;
    // return_val = std::max(return_val, checkWithHostGrid<Grid, SIMPLEX>());
    std::cout << "Test for SubGrid<ALUGrid<3, 3, simplex, nonconforming>> is"
                 "currently disabled because the checkIndexSet test fails for"
                 "this. This seems to be a known problem, see"
                 "dune-grid/dune/grid/test/gridcheck.hh:949ff:"
                 "\"note that for some grid this might fail...\"" << std::endl;
    // TODO: workaround (or fix possible underlying ALUGrid bug?)
  }
  { // simplex conforming
    // using Grid = Dune::ALUGrid<3, 3, Dune::simplex, Dune::conforming>;
    // return_val = std::max(return_val, checkWithHostGrid<Grid, SIMPLEX>());
    std::cout << "Test for SubGrid<ALUGrid<2, 2, simplex, conforming>> is"
                 "currently disabled" << std::endl;
    // FAILS ALUGRID ASSERTION, might relate to corresponding problem in 2D
  }
  { // cube nonconforming
     using Grid = Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>;
     return_val = std::max(return_val, checkWithHostGrid<Grid, CUBE>());
  }

  std::cout << "Test for SubGrid with ALUGrid "
            << (return_val == 0 ? "passed." : "failed.") << std::endl;

  return static_cast<int>(return_val);
#else
  std::cout << "Skip tests for ALUGrid, because it wasn't' found." << std::endl;
  return 0;
#endif
} catch (Dune::Exception e) {
  std::cout << e << std::endl;
  return 1;
}
